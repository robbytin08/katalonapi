import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import listobject.pageLogin
import action.actionPage
import halper.katalon

Mobile.startApplication(GlobalVariable.APK, false)
Mobile.delay(8)
Mobile.tap(katalon.testObject("xpath", String.format(pageLogin.ButtonMasukOpenAPI)), 0)
Mobile.setText(katalon.testObject("xpath", String.format(pageLogin.FieldInputPhoneNumber)),GlobalVariable.UserLogin, 0)
Mobile.setText(katalon.testObject("xpath", String.format(pageLogin.FieldInputPIN)), '111111', 0)
Mobile.tap(katalon.testObject("xpath", String.format(pageLogin.ButtonLogin)), 0)
Mobile.delay(10)